package com.araido.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.beans.factory.annotation.Autowired;

@Controller
public class HomeController {

    @Autowired
    private CustomerRepository repository;

    @GetMapping("/app")
    public String greeting(String name, Model model) {
        model.addAttribute("customers", repository.findAll());
        return "home";
    }

}